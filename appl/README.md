Application
===========

In this module the different application parts resides:
- Information model (JPA entities)
- Services (EJB:s, JAX-WS and JAX-RS modules)
- User interface(s)

It also contains a module which builds the EAR.