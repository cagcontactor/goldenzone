/*
 * Created by Daniel Marell 13-12-15 18:45
 */
package com.caglabs.goldenzone.common;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TestUtil {
    private TestUtil() {
    }

    public static Date createDate(String iso8601DateAndTime) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            return df.parse(iso8601DateAndTime);
        } catch (ParseException e) {
            throw new IllegalArgumentException("Cannot parse data and time string:" + iso8601DateAndTime + ":" + e.getMessage());
        }
    }
}
