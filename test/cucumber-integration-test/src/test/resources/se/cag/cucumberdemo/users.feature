Feature: User handling

  Background: Adding users
    Given Users calvin and hobbes exist

  Scenario: Add a single user
    When A user snoopy is added with display name Snoopy
    Then Number of users are 3
    And  user calvin exist
    And  user hobbes exist
    And  user snoopy
