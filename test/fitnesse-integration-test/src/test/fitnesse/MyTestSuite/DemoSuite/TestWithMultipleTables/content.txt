!contents -R2 -g -p -f -h

|myFixtures.TalkHello              |
|name  |realName        |addUser()?|
|anna  |Anna Andersson  |/__VOID__/|
|annika|Annika Bengtsson|/__VOID__/|

|myFixtures.TalkHello                                          |
|name  |sayHello()?                                            |
|anna  |=~/Hello Anna Andersson \(id=\d+\)!!! The time is .*/  |
|annika|=~/Hello Annika Bengtsson \(id=\d+\)!!! The time is .*/|

|myFixtures.TalkHello|
|name |removeUser()? |
|anna |/__VOID__/    |

|myFixtures.TalkHello                                          |
|name  |sayHello()?                                            |
|anna  |Exception                                              |
|annika|=~/Hello Annika Bengtsson \(id=\d+\)!!! The time is .*/|

|myFixtures.TalkHello|
|name  |removeUser()?|
|annika|/__VOID__/   |

|myFixtures.TalkHello|
|name   |sayHello()? |
|anna   |Exception   |
|annika |Exception   |
