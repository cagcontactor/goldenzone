package myFixtures;

import com.caglabs.goldenzone.helloclient.HelloBean;
import com.caglabs.goldenzone.helloclient.HelloException_Exception;
import com.caglabs.goldenzone.helloclient.HelloServiceLocator;

public class sayHello {
    public String name;
    public void setName(String name) { this.name = name; };
    public String sayHello() {
        HelloBean hello = new HelloServiceLocator().getService();
        try {
            System.out.println("Jag får argumentet '" + name + "'");
            String greeting = hello.sayHello(name);
            return greeting;
        } catch (HelloException_Exception e) {
           System.out.println("Caught exception: " + e.getMessage());
           // e.printStackTrace(System.out);
	   return "Exception";
        }
    }
}
