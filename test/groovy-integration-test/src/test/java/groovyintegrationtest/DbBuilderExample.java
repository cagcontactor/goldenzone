/*
 * Created by Daniel Marell 13-11-18 5:27 PM
 */
package groovyintegrationtest;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class DbBuilderExample {
    Connection conn;
    Statement stmt;

    @Before
    public void setUp() throws Exception {
        Class.forName("com.mysql.jdbc.Driver");
        conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/goldenzone_it", "test", "test");
        stmt = conn.createStatement();
    }

    @After
    public void tearDown() throws Exception {
        stmt.close();
        conn.close();
    }

    @Test
    public void shouldLoad_3_0() throws SQLException {
        deleteAllGoldenzoneUsers();
        new DbLoader().load(stmt);
    }

    @Test
    public void shouldLoad_3_1() throws SQLException {
        deleteAllGoldenzoneUsers();
        new DbLoader()
                .withUser(new User("daniel", "Daniel Marell"))
                .load(stmt);
    }

    @Test
    public void shouldLoad_2_2() throws SQLException {
        deleteAllGoldenzoneUsers();
        new DbLoader().withNumUsers(2)
                .withUser(new User("daniel", "Daniel Marell"))
                .withUser(new User("anders", "Anders Ekdahl"))
                .load(stmt);
    }

    private void deleteAllGoldenzoneUsers() throws SQLException {
        stmt.execute("delete from goldenzone_user");
    }
}

