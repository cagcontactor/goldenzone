/*
 * Created by Daniel Marell 13-11-18 8:17 PM
 */
package groovyintegrationtest;

import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class DbLoader {
    int numUsers = 3;
    List<User> users = new ArrayList<>();

    public DbLoader() {
    }

    public DbLoader withNumUsers(int numUsers) {
        this.numUsers = numUsers;
        return this;
    }

    public DbLoader withUser(User user) {
        users.add(user);
        return this;
    }

    public void load(Statement stmt) throws SQLException {
        for (int i = 0; i < numUsers; ++i) {
            users.add(new User("kalle" + i, "Kalle #" + i));
        }
        for (User user : users) {
            final String sql = String.format("insert into goldenzone_user (name, display_name) values ('%s', '%s')",
                    user.getName(), user.getDisplayName());
            stmt.execute(sql);
        }

    }
}
