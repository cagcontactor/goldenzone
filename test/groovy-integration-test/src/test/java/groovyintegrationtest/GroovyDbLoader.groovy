package groovyintegrationtest

import groovy.sql.Sql

/*
 * Created by Daniel Marell 13-11-18 8:55 PM
 */
class GroovyDbLoader {
    def numUsers = 3
    def users = []

    GroovyDbLoader withNumUsers(int numUsers) {
        this.numUsers = numUsers
        return this
    }

    GroovyDbLoader withUser(User user) {
        users.add(user)
        return this
    }

    void load(Sql sql) {
        for (int i = 0; i < numUsers; ++i) {
            users.add(new User("kalle" + i, "Kalle #" + i))
        }
        for (User user : users) {
            sql.executeInsert(String.format("insert into goldenzone_user (name, display_name) values ('%s', '%s')",
                    user.getName(), user.getDisplayName()))
        }
    }
}
