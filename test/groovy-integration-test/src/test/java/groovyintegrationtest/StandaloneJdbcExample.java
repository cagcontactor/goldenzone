/*
 * Created by Daniel Marell 13-11-18 5:27 PM
 */
package groovyintegrationtest;

import org.junit.Test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class StandaloneJdbcExample {
    @Test
    public void test() throws Exception {
        Class.forName("com.mysql.jdbc.Driver");
        Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/goldenzone_it", "test", "test");
        Statement stmt = conn.createStatement();
        String sql = "select * from goldenzone_user";
        ResultSet rs = stmt.executeQuery(sql);
        while (rs.next()) {
            int id = rs.getInt("id");
            String name = rs.getString("name");
            String displayName = rs.getString("display_name");
            System.out.printf("%d %s %s\n", id, name, displayName);
        }
        stmt.close();
        conn.close();
    }
}
