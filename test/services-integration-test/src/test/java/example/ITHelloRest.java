/*
 * Created by Daniel Marell 13-11-14 11:07 PM
 */
package example;

import com.caglabs.goldenzone.helloclient.HelloRestClient;
import com.caglabs.goldenzone.infomodel.dbloader.AbstractDBLoaderBase;
import com.caglabs.goldenzone.infomodel.dbloader.DBUtil;
import com.caglabs.goldenzone.infomodel.dbloader.ExampleInfoModelDBLoader;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.core.util.MultivaluedMapImpl;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;

import static junit.framework.Assert.assertNull;
import static junit.framework.Assert.fail;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class ITHelloRest {
    private HelloRestClient restClient = new HelloRestClient();

    @Before
    public void setup() throws AbstractDBLoaderBase.DBLoadException {
        // Populate DB before each test
        new ExampleInfoModelDBLoader().execute();
    }

    @After
    public void teardown() {
        // Drop DB after each test
        DBUtil.dropDB();
    }

    @Test
    public void shouldListUserNames() {
        List<String> users = restClient.getUserNames();
        assertThat(users.size(), is(3));
        assertTrue(users.contains("kalle0"));
        assertTrue(users.contains("kalle1"));
        assertTrue(users.contains("kalle2"));
    }

    @Test
    public void shouldAddAndUpdateUser() throws Exception {
        // First verify that user does exist
        if (restClient.getDisplayName("newton") != null) {
            fail("Expected null");
        }

        // Create user
        restClient.addUser("newton", "Isaac Newton");

        // Verify that user now exist
        assertThat(restClient.getDisplayName("newton"), is("Isaac Newton"));

        // Operation should be idempotent
        restClient.addUser("newton", "Isaac Newton");

        // Verify that user now exist
        assertThat(restClient.getDisplayName("newton"), is("Isaac Newton"));

        // Operation update user
        restClient.addUser("newton", "Isaac Newton again");

        // Verify update
        assertThat(restClient.getDisplayName("newton"), is("Isaac Newton again"));
    }

    @Test
    public void shouldRemoveUser() throws Exception {
        // First verify that user does exist
        assertThat(restClient.getDisplayName("kalle2"), is("Kalle #2"));

        // Remove user
        restClient.removeUser("kalle2");

        // Verify that user does exist
        assertNull(restClient.getDisplayName("kalle2"));
    }
}

