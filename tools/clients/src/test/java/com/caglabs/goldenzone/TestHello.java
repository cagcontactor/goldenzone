/*
 * Created by Daniel Marell 13-10-27 7:46 PM
 */
package com.caglabs.goldenzone;

import com.caglabs.goldenzone.helloclient.HelloBean;
import com.caglabs.goldenzone.helloclient.HelloException_Exception;
import com.caglabs.goldenzone.helloclient.HelloServiceLocator;

public class TestHello {
    public static void main(String[] args) {
        HelloBean hello = new HelloServiceLocator().getService();
        try {
            String greeting = hello.sayHello("kalle");
            System.out.println("Greeting=" + greeting);
        } catch (HelloException_Exception e) {
            System.out.println("Caught exception: " + e.getMessage());
        }
    }
}
