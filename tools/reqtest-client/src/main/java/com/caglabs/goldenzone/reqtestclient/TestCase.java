/*
 * Created by Daniel Marell 13-12-15 16:29
 */
package com.caglabs.goldenzone.reqtestclient;

import nu.xom.*;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import java.util.*;

/**
 * Represents a ReQtest test case.
 */
//@XmlRootElement(namespace = "http://reqtest.com/API/V1/")
//@XmlSeeAlso(TestCaseField.class)
public class TestCase {
    private int id;
    private boolean archived;
    private int attachmentCount;
    private Date changedDate;
    private int commentCount;
    private String createdBy;
    private int createdByUserId;
    private Date createdDate;
    private int customId;
    private List<TestCaseField> fields = new ArrayList<>();

    public TestCase() {
    }

    public TestCase(int id, boolean archived, int attachmentCount, Date changedDate, int commentCount, String createdBy,
                    int createdByUserId, Date createdDate, int customId, List<TestCaseField> fields) {
        this.id = id;
        this.archived = archived;
        this.attachmentCount = attachmentCount;
        this.changedDate = changedDate;
        this.commentCount = commentCount;
        this.createdBy = createdBy;
        this.createdByUserId = createdByUserId;
        this.createdDate = createdDate;
        this.customId = customId;
        this.fields = fields;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isArchived() {
        return archived;
    }

    public void setArchived(boolean archived) {
        this.archived = archived;
    }

    public int getAttachmentCount() {
        return attachmentCount;
    }

    public void setAttachmentCount(int attachmentCount) {
        this.attachmentCount = attachmentCount;
    }

    public Date getChangedDate() {
        return changedDate;
    }

    public void setChangedDate(Date changedDate) {
        this.changedDate = changedDate;
    }

    public int getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(int commentCount) {
        this.commentCount = commentCount;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public int getCreatedByUserId() {
        return createdByUserId;
    }

    public void setCreatedByUserId(int createdByUserId) {
        this.createdByUserId = createdByUserId;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public int getCustomId() {
        return customId;
    }

    public void setCustomId(int customId) {
        this.customId = customId;
    }

//    @XmlElement(name = "field")
    public List<TestCaseField> getFields() {
        return fields;
    }

    public void setFields(List<TestCaseField> fields) {
        this.fields = fields;
    }

    public TestCaseField getField(String fieldName) {
        for (TestCaseField f : fields) {
            if (f.getFieldName().equals(fieldName)) {
                return f;
            }
        }
        return null;
    }

    public String getFieldValue(String fieldName) {
        TestCaseField f = getField(fieldName);
        if (f != null) {
            return f.getValue();
        }
        return null;
    }

//    public static TestCase parseTestCase(Element element) throws ParsingException {
//        int id = ParseHelper.parseIntElement(element, "id", false);
//        boolean archived = ParseHelper.parseBooleanElement(element, "archived", false);
//        int attachmentCount = ParseHelper.parseIntElement(element, "attachmentCount", false);
//        Date changedDate = ParseHelper.parseDateElement(element, "changedDate", true);
//        int commentCount = ParseHelper.parseIntElement(element, "commentCount", false);
//        String createdBy = ParseHelper.parseStringElement(element, "createdBy", false);
//        int createdByUserId = ParseHelper.parseIntElement(element, "createdByUserId", false);
//        Date createdDate = ParseHelper.parseDateElement(element, "createdDate", false);
//        int customId = ParseHelper.parseIntElement(element, "customId", false);
//        Element fieldValuesElem = element.getFirstChildElement("fieldValues", ParseHelper.NS);
//        List<TestCaseField> fields = new ArrayList<>();
//        if (fieldValuesElem != null) {
//            Elements fieldValuesElems = fieldValuesElem.getChildElements("fieldValue", ParseHelper.NS);
//            for (int i = 0; i < fieldValuesElems.size(); ++i) {
//                fields.add(TestCaseField.parseFieldValue(fieldValuesElems.get(i)));
//            }
//        }
//        return new TestCase(id, archived, attachmentCount, changedDate, commentCount, createdBy, createdByUserId,
//                createdDate, customId, fields);
//    }
}
