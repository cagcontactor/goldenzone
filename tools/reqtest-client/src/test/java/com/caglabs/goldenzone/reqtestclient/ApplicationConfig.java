/*
 * Created by Daniel Marell 13-12-23 09:55
 */
package com.caglabs.goldenzone.reqtestclient;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

//@ApplicationPath("rs")
public class ApplicationConfig extends Application {
    @Override
    public Set<Class<?>> getClasses() {
        return new HashSet<Class<?>>() {{
            add(ReQtestResource.class);
        }};
    }
}