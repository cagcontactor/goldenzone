/*
 * Created by Daniel Marell 13-12-22 23:13
 */
package com.caglabs.goldenzone.reqtestclient;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;

@Path("{projectId}")
public class ReQtestResource {
    @GET
    @Path("/testcases")
    @Produces(MediaType.APPLICATION_XML)
    public Response getTestCases(@PathParam("projectId") long projectId) {
        TestCases testCases = new TestCases(projectId, new ArrayList<TestCase>());
        return Response.ok(testCases).build();
    }

    @Path("testCases/{testCaseId}")
    @PUT
    @Consumes(MediaType.APPLICATION_XML)
    public void updateTestCase(@PathParam("projectId") long projectId, long testCaseId) {
        System.out.println("updateTestCase,projectId=" + projectId + ",testCaseId=" + testCaseId);
//        logger.fine("setDisplayName,name=" + name + ",displayName=" + displayName);
//        Hello hello = lookupHelloService();
//        try {
//            hello.updateUser(name, displayName);
//        } catch (Hello.HelloException e) {
//            hello.addUser(name, displayName);
//        }
    }
}
